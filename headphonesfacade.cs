using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class headphonesfacade
{
    headphonesmodel model;
    headphonestype type;
    headphonesrange range;
    headphonesimpedance impedance;

    public headphonesfacade()
    {
        model = new headphonesmodel();
        type = new headphonestype();
        range = new headphonesrange();
        impedance = new headphonesimpedance();
    }

    public void createcompleteheadphones()
    {
        Console.WriteLine(" Creating a Headphones");
        System.Threading.Thread.Sleep(1600);
        model.setmodel();
        System.Threading.Thread.Sleep(1600);
        type.settype();
        System.Threading.Thread.Sleep(1600);
        range.setrange();
        System.Threading.Thread.Sleep(1600);
        impedance.setimpedance();
        System.Threading.Thread.Sleep(1600);
        Console.WriteLine(" Headphones creation complete");
    }
}